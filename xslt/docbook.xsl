<?xml version="1.0" encoding="UTF-8"?>
<!--  Convertisseur de documents article-fb en DocBook journalarticle-->
<xsl:stylesheet version="1.0" 
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:xlink="http://www.w3.org/1999/xlink"
  xmlns:date="http://exslt.org/dates-and-times"
  extension-element-prefixes="date"
  xmlns="http://docbook.org/ns/docbook">
  
  <xsl:output method="xml" encoding="UTF-8" indent="yes"/>
  <xsl:strip-space elements="*"/>
  
  <xsl:attribute-set name="db5.attr">
    <xsl:attribute name="version">5</xsl:attribute>
    <xsl:attribute name="class">journalarticle</xsl:attribute>
    <xsl:attribute name="xml:id">
      <xsl:choose>
        <xsl:when test="@xml:id">
          <xsl:value-of select="@xml:id"/>
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="generate-id()"/>
          <!-- FIXME: autoIDentifier -->
        </xsl:otherwise>
      </xsl:choose>
    </xsl:attribute>
    <xsl:attribute name="xml:lang">
      <xsl:choose>
        <xsl:when test="@xml:lang">
          <xsl:value-of select="@xml:lang"/>
        </xsl:when>
        <xsl:otherwise>en</xsl:otherwise>
      </xsl:choose>
    </xsl:attribute>
  </xsl:attribute-set>
  
  <xsl:variable name="chemin.images.relatif">img/</xsl:variable>
  
  <xsl:template match="article">
    <article xsl:use-attribute-sets="db5.attr">
      <xsl:apply-templates/>
    </article>
  </xsl:template>
  
  <!-- traitement par défaut -->
  <xsl:template match="*">
    <xsl:param name="message"
      select="concat('pas de modèle pour ',local-name())"
    />
    <xsl:message>
      <xsl:value-of select="$message"/>
    </xsl:message>
    <xsl:comment>
      <xsl:value-of select="$message"/>
    </xsl:comment>
  </xsl:template>
  
  <!-- méta-informations transformées -->
  <xsl:template match="tête" name="info">
    <info>
      <xsl:for-each select="parent::article/@url">
        <xsl:call-template name="gen-bibliosource">
          <xsl:with-param name="classe" select="'uri'"/>
          <xsl:with-param name="contenu" select="."/>
        </xsl:call-template>
      </xsl:for-each>
      <xsl:apply-templates/>
    </info>
  </xsl:template>
  
  <xsl:template match="auteurs">
    <authorgroup>
      <xsl:apply-templates/>
    </authorgroup>
  </xsl:template>
  
  <xsl:template match="auteur">
    <author>
      <personname>
        <xsl:apply-templates/>
      </personname>
    </author>
  </xsl:template>
  
  <xsl:template match="éditeur">
    <!-- fixme: supprimer la correspondance obsolète -->
    <editor>
      <orgname>
        <xsl:apply-templates/>
      </orgname>
    </editor>
    <xsl:if test="@numéro">
      <issuenum>
        <xsl:value-of select="@numéro"/>
      </issuenum>
    </xsl:if>
  </xsl:template>
  
  <xsl:template match="publicateur">
    <!-- fixme: supprimer la correspondance obsolète -->
    <publishername>
      <xsl:apply-templates/>
    </publishername>
    <xsl:if test="@numéro">
      <issuenum>
        <xsl:value-of select="normalize-space(@numéro)"/>
      </issuenum>
    </xsl:if>
    <xsl:for-each select="@issn">
      <xsl:call-template name="gen-bibliosource">
        <xsl:with-param name="classe" select="'issn'"/>
        <xsl:with-param name="contenu" select="."/>
      </xsl:call-template>
    </xsl:for-each>
  </xsl:template>
  
  <xsl:template match="contributeur[1]">
    <!-- fixme: supprimer la correspondance obsolète -->
    <xsl:param name="reviewintro">Contributeur : </xsl:param>
    <releaseinfo>
      <xsl:value-of select="concat(     $reviewintro , normalize-space()    )"/>
      <xsl:for-each select="                 following-sibling::contributeur">
        <xsl:value-of select="concat(      ', ' , normalize-space()     )"/>
      </xsl:for-each>
    </releaseinfo>
  </xsl:template>
  
  <xsl:template match="contributeur">
    <!-- fixme: supprimer la correspondance obsolète -->
    <!-- géré par réviseur[1] -->
  </xsl:template>
  
  <xsl:template match="résumé">
    <abstract>
      <simpara>
        <xsl:apply-templates/>
      </simpara>
    </abstract>
  </xsl:template>
  
  <xsl:template match="sujets">
    <subjectset>
      <xsl:apply-templates/>
    </subjectset>
  </xsl:template>
  
  <xsl:template match="sujet">
    <subject>
      <subjectterm>
        <xsl:apply-templates/>
      </subjectterm>
    </subject>
  </xsl:template>
  
  <xsl:template name="gen-bibliosource">
    <xsl:param name="classe"/>
    <xsl:param name="contenu"/>
    <bibliosource>
      <xsl:attribute name="class">
        <xsl:value-of select="$classe"/>
      </xsl:attribute>
      <xsl:if test="$classe='other'">
        <xsl:attribute name="otherclass">
          <xsl:value-of select="$contenu"/>
        </xsl:attribute>
      </xsl:if>
      <xsl:value-of select="$contenu"/>
    </bibliosource>
  </xsl:template>
  
  <!-- types blocs -->
  <xsl:template match="sections | corps">
    <xsl:apply-templates/>
  </xsl:template>
  
  <xsl:template match="section">
    <simplesect>
      <info>
        <xsl:apply-templates select="titre"/>
        <xsl:if test="not(titre)">
          <title>
            <xsl:comment>untitled <xsl:number/></xsl:comment>
          </title>
        </xsl:if>
        <xsl:apply-templates select="média | publication"/>
      </info>
      <xsl:apply-templates select="paragraphe"/>
    </simplesect>
  </xsl:template>
  
  <xsl:template match="section[contains(@class,'enca')]">
    <sidebar>
      <xsl:apply-templates select="titre"/>
      <xsl:apply-templates select="paragraphe"/>
    </sidebar>
  </xsl:template>
  
  <xsl:template match="titre">
    <title>
      <xsl:apply-templates/>
    </title>
  </xsl:template>
  
  <xsl:template match="paragraphe | p">
    <simpara>
      <xsl:apply-templates/>
    </simpara>
  </xsl:template>
  
  <!-- objets multimédia -->
  <xsl:template match="média | image">
    <xsl:param name="info">
      <xsl:apply-templates select="auteur"/>
      <xsl:apply-templates select="légende | description" mode="info"/>
      <!-- FIXME appliquer davantage de modèles -->
    </xsl:param>
    <mediaobject>
      <xsl:if test="$info != ''">
        <info>
          <xsl:copy-of select="$info"/>
        </info>
      </xsl:if>
      <xsl:call-template name="gen-imageobject"/>
      <xsl:apply-templates select="légende | description" mode="caption"/>
    </mediaobject>
  </xsl:template>
  
  <xsl:template name="gen-imageobject">
    <xsl:param name="url" select="descendant-or-self::*/@url"/>
    <xsl:param name="largeur" select="descendant-or-self::*/@largeur"/>
    <xsl:param name="hauteur" select="descendant-or-self::*/@hauteur"/>
    <imageobject>
      <imagedata>
        <xsl:attribute name="fileref">
          <xsl:if test="not(
            contains($url,'https://') or
            contains($url,'http://') or
            contains($url,'www.') or
            contains($url,'file://')
          )">
            <xsl:value-of
              select="normalize-space($chemin.images.relatif)"/>
          </xsl:if>
          <xsl:value-of select="$url"/>
        </xsl:attribute>
        <xsl:if test="$largeur">
          <xsl:attribute name="width">
            <xsl:value-of select="$largeur"/>
            <xsl:if test="not(
              contains($largeur,'px') or
              contains($largeur,'pt') or
              contains($largeur,'in') or
              contains($largeur,'mm') or
              contains($largeur,'cm')
            )">px</xsl:if>
          </xsl:attribute>
        </xsl:if>
        <xsl:if test="$hauteur">
          <xsl:attribute name="depth">
            <xsl:value-of select="$hauteur"/>
            <xsl:if test="not(
              contains($hauteur,'px') or
              contains($hauteur,'pt') or
              contains($hauteur,'in') or
              contains($hauteur,'mm') or
              contains($hauteur,'cm')
            )">px</xsl:if>
          </xsl:attribute>
        </xsl:if>
      </imagedata>
    </imageobject>
  </xsl:template>
  
  <xsl:template match="description | légende" mode="info">
    <abstract>
      <simpara>
        <xsl:apply-templates/>
      </simpara>
    </abstract>
  </xsl:template>
  
  <xsl:template match="description | légende" mode="caption">
    <caption>
      <simpara>
        <xsl:apply-templates/>
      </simpara>
    </caption>
  </xsl:template>
  
  <!-- types alignés -->
  <xsl:template match="définition">
    <xsl:choose>
      <xsl:when test="sujet">
        <termdef>
          <xsl:apply-templates/>
        </termdef>
      </xsl:when>
      <xsl:when test="@abr">
        <!-- définition d'abbréviation -->
        <abbrev>
          <xsl:value-of select="@abr"/>
          <alt>
            <xsl:apply-templates/>
          </alt>
        </abbrev>
      </xsl:when>
      <xsl:otherwise>
        <xsl:call-template name="note"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
  
  <xsl:template match="définition/sujet">
    <firstterm>
      <xsl:apply-templates/>
    </firstterm>
  </xsl:template>
  
  <xsl:template match="note" name="note">
    <footnote>
      <simpara>
        <xsl:apply-templates/>
      </simpara>
    </footnote>
  </xsl:template>
  
  <xsl:template match="lien">
    <link>
      <xsl:attribute name="xlink:href">
        <xsl:value-of select="@url"/>
      </xsl:attribute>
      <xsl:apply-templates/>
    </link>
  </xsl:template>
  
  <xsl:template match="propriété">
    <copyright>
      <year>
        <xsl:choose>
          <xsl:when test="@année">
            <xsl:value-of select="@année"/>
          </xsl:when>
          <xsl:otherwise>
            <xsl:value-of select="date:year()"/>
          </xsl:otherwise>
        </xsl:choose>
      </year>
      <holder>
        <xsl:value-of select="normalize-space()"/>
      </holder>
      <!-- FIXME indiquer une licence en DocBook-->
    </copyright>
  </xsl:template>
  
  <xsl:template match="publication">
    <xsl:variable name="procdate" select="substring-before(date:date(),'+')"/>
    <xsl:variable name="userdate" select="date:date(normalize-space())"/>
    <pubdate>
      <xsl:choose>
        <xsl:when test="$userdate != ''">
          <xsl:value-of select="$userdate"/>
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="$procdate"/>
        </xsl:otherwise>
      </xsl:choose>
    </pubdate>
  </xsl:template>
  
  <xsl:template match="emphase">
    <emphasis>
      <xsl:apply-templates/>
    </emphasis>
  </xsl:template>
  
  <xsl:template match="section/citation">
    <blockquote>
      <xsl:apply-templates/>
    </blockquote>
  </xsl:template>
  
  <xsl:template match="citation">
    <quote>
      <xsl:apply-templates/>
    </quote>
  </xsl:template>

</xsl:stylesheet>

