<?xml version="1.0" encoding="UTF-8"?>

<xsl:transform version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:fo="http://www.w3.org/1999/XSL/Format"
  xmlns:xlink="http://www.w3.org/1999/xlink"
  xmlns:rx="http://www.renderx.com/XSL/Extensions"
  xmlns:svg="http://www.w3.org/2000/svg"
  xmlns:db="http://docbook.org/ns/docbook"
  xmlns:exsl="http://exslt.org/common"
  xmlns:date="http://exslt.org/dates-and-times">

  <xsl:import href="fo.styles.xsl"/>

  <xsl:output method="xml" encoding="UTF-8" indent="no"/>

  <xsl:strip-space elements="*"/>
  <xsl:preserve-space elements="db:literallayout db:programlisting"/>


  <!-- == variables ==  -->

  <xsl:key name="gid" match="//*[@xml:id]" use="@xml:id"/>

  <xsl:variable name="xslt.nom">journalarticle</xsl:variable>

  <xsl:variable name="xslt.auteur">frabad</xsl:variable>

  <xsl:variable name="xslt.url">http://gitlab.com/frabad/journalarticle</xsl:variable>

  <xsl:variable name="xslt.procdate" select="concat( date:year() , '.' , date:day-in-year() )"/>

  <xsl:variable name="msg.doc-refusé">ERREUR: Le document d'entrée n'est pas un article de presse. Transformation annulée.</xsl:variable>

  <!-- méta -->

  <xsl:variable name="doc.title">
    <xsl:apply-templates select="/db:article/db:info/db:title"/>
    <xsl:variable name="subtitle">
      <xsl:apply-templates select="//db:subtitle[1]"/>
    </xsl:variable>
    <xsl:if test="$subtitle !=''">
      <xsl:text> - </xsl:text>
      <xsl:value-of select="$subtitle"/>
    </xsl:if>
  </xsl:variable>

  <xsl:variable name="doc.author">
    <xsl:apply-templates
      select="(
                //db:authorgroup | // db:editor | //db:publishername | //db:holder
            )[1]"
    />
  </xsl:variable>

  <xsl:variable name="doc.keywords">
    <xsl:apply-templates select="//db:keywordset"/>
  </xsl:variable>

  <xsl:variable name="doc.subject">
    <xsl:apply-templates select="//db:subjectset"/>
  </xsl:variable>

  <xsl:variable name="doc.processor">
    <xsl:value-of select="$xslt.nom"/>
    <!--
        <xsl:text> V</xsl:text>
        <xsl:value-of select="$VERSION"/>
        -->
  </xsl:variable>

  <xsl:variable name="doc.copyright">
    <xsl:apply-templates select="//db:copyright[1]" mode="info-visible"/>
  </xsl:variable>

  <!--FIXME: ajouter support de @revision
    <xsl:variable name="doc.revision">
        <xsl:if test="normalize-space(//@revision[1]) != ''">
            <xsl:call-template name="gentext">
                <xsl:with-param name="key" select="'Revision'"/>
            </xsl:call-template>
            <xsl:text> </xsl:text>
            <xsl:choose>
                <xsl:when test="'today'">
                    <xsl:value-of select="$xslt.procdate"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="normalize-space()"/>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:if>
    </xsl:variable>
-->

  <!-- == paramètres == -->

  <xsl:param name="prm.signature" select="false()"/>

  <xsl:param name="prm.signature.svg" select="true()"/>

  <xsl:param name="prm.url.visible" select="false()"/>

  <xsl:param name="prm.signets" select="true()"/>

  <xsl:param name="prm.méta.DC" select="true()"/>

  <xsl:param name="prm.méta.XEP" select="false()"/>

  <xsl:param name="prm.césure" select="false()"/>

  <xsl:param name="prm.langue" select="/db:article/@xml:lang"/>

  <xsl:param name="prm.classe.journalarticle"
    select="boolean(//db:article[@class='journalarticle'])"/>

  <!-- == correspondances == -->

  <xsl:template match="*">
    <xsl:variable name="debug" select="concat('FIXME: ',local-name())"/>
    <xsl:message>
      <xsl:value-of select="$debug"/>
    </xsl:message>
    <xsl:comment>
            <xsl:value-of select="$debug"/>
            <xsl:apply-templates/>
        </xsl:comment>
  </xsl:template>

  <xsl:template match="/">

    <xsl:element name="fo:root">

      <xsl:copy-of select="$prm.langue"/>

      <xsl:call-template name="gen-méta"/>

      <xsl:element name="fo:layout-master-set">

        <xsl:element name="fo:simple-page-master" use-attribute-sets="sty.arch-page">
          <xsl:attribute name="master-name">A4</xsl:attribute>

          <!-- FIXME: placer les attributs de page en styles ou en variables -->
          <fo:region-body column-count="2" margin-bottom="10mm" margin-top="10mm" column-gap="5mm"/>
          <fo:region-before extent="6mm"/>
          <fo:region-after extent="6mm"/>

        </xsl:element>

        <fo:page-sequence-master master-name="sequence-A4">
          <fo:repeatable-page-master-reference master-reference="A4"/>
        </fo:page-sequence-master>

      </xsl:element>

      <xsl:if test="$prm.signets = true()">
        <xsl:element name="fo:bookmark-tree">
          <xsl:apply-templates mode="gen-signet" select="db:article"/>
        </xsl:element>
      </xsl:if>

      <xsl:choose>
        <xsl:when test="$prm.classe.journalarticle = false()">
          <xsl:message>
            <xsl:value-of select="normalize-space($msg.doc-refusé)"/>
          </xsl:message>
        </xsl:when>

        <xsl:otherwise>
          <xsl:apply-templates/>
        </xsl:otherwise>
      </xsl:choose>

    </xsl:element>

  </xsl:template>


  <!-- === architecture FO === -->

  <xsl:template match="db:article">

    <fo:page-sequence master-reference="sequence-A4" xsl:use-attribute-sets="sty.défaut">
      <xsl:attribute name="hyphenate">
        <xsl:choose>
          <xsl:when test="$prm.césure">true</xsl:when>
          <xsl:otherwise>false</xsl:otherwise>
        </xsl:choose>
      </xsl:attribute>

      <xsl:call-template name="gen-static-content"/>

      <fo:flow flow-name="xsl-region-body">

        <xsl:call-template name="gen-tête"/>

        <xsl:call-template name="gen-corps"/>

        <xsl:call-template name="gen-signature"/>

        <xsl:call-template name="gen-bloc-notes"/>

        <fo:block id="dernière-page">
          <xsl:comment> dernière page </xsl:comment>
        </fo:block>

      </fo:flow>

    </fo:page-sequence>

  </xsl:template>


  <!-- === méta-informations === -->

  <xsl:template match="db:article/db:info">
    <!--voir gen-tête-->
  </xsl:template>

  <xsl:template match="db:section/db:info | db:simplesect/db:info | db:sidebar/db:info">
    <xsl:apply-templates/>
  </xsl:template>

  <!--FIXME: remplacer les élements DocBook par des éléments Dublin Core
    http://www.docbook.org/tdg5/publishers/5.0/en/html/ch02.html#ch-gsxml.3
    xmlns:dc="http://purl.org/dc/terms/"
    -->
  <xsl:template mode="info-visible"
    match="
        db:pubdate |
        db:authorgroup |
        db:editor |
        db:publishername |
        db:copyright |
        db:subjectset |
        db:keywordset">
    <xsl:param name="meta-nom">
      <xsl:call-template name="gen-locale">
        <xsl:with-param name="nom" select="local-name(.)"/>
      </xsl:call-template>
    </xsl:param>
    <xsl:param name="meta-valeur">
      <xsl:apply-templates select="."/>
    </xsl:param>
    <xsl:element name="fo:inline" use-attribute-sets="sty.métadonnée">
      <xsl:value-of select="$meta-nom"/>
      <xsl:copy-of select="$meta-valeur"/>
    </xsl:element>
  </xsl:template>

  <xsl:template match="db:bibliosource" mode="info-visible">
    <xsl:param name="meta-nom">
      <xsl:call-template name="gen-locale">
        <xsl:with-param name="nom">
          <xsl:choose>
            <xsl:when test="@otherclass">
              <xsl:value-of select="@otherclass"/>
            </xsl:when>
            <xsl:otherwise>
              <xsl:value-of select="@class"/>
            </xsl:otherwise>
          </xsl:choose>
        </xsl:with-param>
      </xsl:call-template>
    </xsl:param>
    <xsl:param name="meta-valeur">
      <xsl:apply-templates select="self::node()"/>
    </xsl:param>
    <xsl:element name="fo:block" use-attribute-sets="sty.métadonnée-article">
      <xsl:value-of select="concat($meta-nom,$meta-valeur)"/>
    </xsl:element>
  </xsl:template>

  <xsl:template match="db:article/db:info/db:title">
    <xsl:apply-templates/>
  </xsl:template>

  <xsl:template match="db:abstract" mode="info-visible">
    <xsl:apply-templates/>
  </xsl:template>

  <xsl:template match="db:pubdate | db:bibliosource | db:editor">
    <xsl:value-of select="normalize-space()"/>
  </xsl:template>

  <xsl:template match="db:publishername">
    <xsl:variable name="contenu">
      <xsl:apply-templates/>
      <xsl:text> </xsl:text>
      <!-- fixme: gestion de issuenum dans les méta -->
      <xsl:value-of select="parent::db:info/db:issuenum"/>
    </xsl:variable>
    <xsl:value-of select="normalize-space($contenu)"/>
  </xsl:template>

  <xsl:template match="db:authorgroup">
    <xsl:apply-templates/>
  </xsl:template>

  <xsl:template match="db:author">
    <xsl:apply-templates select="db:personname"/>
    <xsl:for-each select="db:email | db:uri">
      <xsl:text> </xsl:text>
      <xsl:apply-templates select="."/>
      <xsl:if test="position() != last()"> |</xsl:if>
    </xsl:for-each>
    <xsl:if test="following-sibling::db:author">, </xsl:if>
  </xsl:template>
  
  <xsl:template match="db:email">
    <xsl:text>&lt;</xsl:text>
      <xsl:call-template name="gen-lien">
        <xsl:with-param name="href" select="concat('mailto:',normalize-space(.))"/>
      </xsl:call-template>
    <xsl:text>&gt;</xsl:text>
  </xsl:template>
  
  <xsl:template match="db:personname | db:orgname">
    <xsl:apply-templates/>
  </xsl:template>
  
  <xsl:template match="db:firstname | db:surname | db:othername">
    <xsl:apply-templates />
    <xsl:if test="position() != last()"> </xsl:if>
  </xsl:template>

  <xsl:template match="db:subjectset | db:keywordset">
    <xsl:apply-templates/>
  </xsl:template>

  <xsl:template match="db:subjectterm">
    <xsl:apply-templates/>
    <xsl:if test="position() != last()"><xsl:text> </xsl:text></xsl:if>
  </xsl:template>

  <xsl:template match="db:subject | db:keyword">
    <xsl:variable name="content">
      <xsl:apply-templates/>
    </xsl:variable>
    <xsl:value-of select="normalize-space($content)" />
    <xsl:if test="following-sibling::db:*[local-name(.)=local-name(current())]">
      <xsl:text>, </xsl:text>
    </xsl:if>
  </xsl:template>

  <xsl:template match="db:copyright">
    <xsl:for-each select="db:year">
      <xsl:text> </xsl:text>
      <xsl:apply-templates/>
      <xsl:if test="following-sibling::db:year">,</xsl:if>
    </xsl:for-each>
    <xsl:text> </xsl:text>
    <xsl:apply-templates select="db:holder"/>
  </xsl:template>

  <xsl:template match="db:holder | db:year">
    <xsl:value-of select="normalize-space()"/>
  </xsl:template>

  <xsl:template match="db:remark">
  </xsl:template>

  <!-- === éléments en ligne  === -->

  <xsl:template match="db:footnote" name="num-footnote">
    <!-- FIXME: espace fine 202F-->
    <xsl:text>&#xa0;</xsl:text>
    <!-- FIXME: créer un lien dynamique -->
    <xsl:element name="fo:inline" use-attribute-sets="sty.numnote">
      <xsl:number count="//db:termdef | //db:footnote" level="any"/>
    </xsl:element>
  </xsl:template>

  <xsl:template match="db:uri">
    <xsl:element name="fo:inline" use-attribute-sets="sty.uri">
      <xsl:apply-templates/>
    </xsl:element>
  </xsl:template>

  <xsl:template match="db:termdef">
    <xsl:element name="fo:inline" use-attribute-sets="sty.emphase">
      <xsl:value-of select="normalize-space(db:firstterm)"/>
    </xsl:element>
    <xsl:call-template name="num-footnote"/>
  </xsl:template>

  <xsl:template match="db:firstterm">
    <xsl:element name="fo:inline" use-attribute-sets="sty.emphase">
      <xsl:apply-templates/>
    </xsl:element>
  </xsl:template>

  <xsl:template match="db:emphasis | db:citetitle | db:application">
    <fo:inline xsl:use-attribute-sets="sty.emphase">
      <xsl:apply-templates/>
    </fo:inline>
  </xsl:template>

  <xsl:template match="db:literal | db:code | db:filename | db:tag">
    <fo:inline xsl:use-attribute-sets="sty.code">
      <xsl:apply-templates/>
    </fo:inline>
  </xsl:template>

  <xsl:template match="db:abbrev">
    <xsl:choose>
      <xsl:when test="db:alt">
        <xsl:value-of select="db:alt"/>
        <xsl:text>&#xa0;(</xsl:text>
        <fo:inline xsl:use-attribute-sets="sty.abbrev">
          <xsl:value-of select="normalize-space(text())"/>
        </fo:inline>
        <xsl:text>)</xsl:text>
      </xsl:when>
      <xsl:otherwise>
        <fo:inline xsl:use-attribute-sets="sty.abbrev">
          <xsl:apply-templates/>
        </fo:inline>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <xsl:template match="db:quote">
    <xsl:param name="q.open">
      <xsl:choose>
        <xsl:when test="lang('fr')">«&#x202F;</xsl:when>
        <xsl:when test="lang('it')">«&#x202F;</xsl:when>
        <xsl:otherwise>&#8220;</xsl:otherwise>
      </xsl:choose>
    </xsl:param>
    <xsl:param name="q.close">
      <xsl:choose>
        <xsl:when test="lang('fr')">&#x202F;»</xsl:when>
        <xsl:when test="lang('it')">&#x202F;»</xsl:when>
        <xsl:otherwise>&#8221;</xsl:otherwise>
      </xsl:choose>
    </xsl:param>
    <xsl:param name="contenu">
      <xsl:apply-templates/>
    </xsl:param>
    <fo:inline xsl:use-attribute-sets="sty.citation">
      <xsl:value-of select="concat($q.open,$contenu,$q.close)"/>
    </fo:inline>
  </xsl:template>

  <xsl:template match="db:blockquote">
    <fo:block xsl:use-attribute-sets="sty.citation.bloc">
      <xsl:apply-templates select="node()[not(self::db:attribution)]"/>
      <xsl:apply-templates select="db:attribution"/>
    </fo:block>
  </xsl:template>

  <xsl:template match="db:attribution">
    <fo:block xsl:use-attribute-sets="sty.citation.attribution">
      <xsl:value-of select="'&#x2014;&#xa0;'"/>
      <xsl:apply-templates/>
    </fo:block>
  </xsl:template>
  
  <xsl:template name="gen-lien"  match="*[@xlink:href]">
    <xsl:param name="href" select="@xlink:href"/>
    <xsl:param name="cible-locale"
      select="key('gid',substring-after($href,'#'))/db:info/db:title"/>
    <xsl:param name="contenu">
      <xsl:choose>
        <xsl:when test="normalize-space(.) != ''">
          <xsl:apply-templates/>
        </xsl:when>
        <xsl:otherwise>
          <xsl:choose>
            <xsl:when test="$cible-locale != ''">
              <xsl:value-of select="$cible-locale"/>
            </xsl:when>
            <xsl:otherwise>
              <xsl:value-of select="$href"/>
            </xsl:otherwise>
          </xsl:choose>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:param>    
    <fo:basic-link>
      <xsl:choose>
        <xsl:when test="$cible-locale">
          <xsl:attribute name="internal-destination">
            <xsl:value-of select="substring-after($href,'#')"/>
          </xsl:attribute>
        </xsl:when>
        <xsl:otherwise>
          <xsl:attribute name="external-destination">
            <xsl:value-of select="concat('url(',$href,')')"/>
          </xsl:attribute>
        </xsl:otherwise>
      </xsl:choose>
      <xsl:element name="fo:inline" use-attribute-sets="sty.lien {local-name(.)}">
        <xsl:copy-of select="$contenu"/>
        <xsl:if test="$prm.url.visible">
          <xsl:value-of select="concat(' [',$href,']')"/>
        </xsl:if>
      </xsl:element>
    </fo:basic-link>
  </xsl:template>


  <!-- === éléments en bloc === -->

  <xsl:template match="db:article/db:info/db:title" mode="info-visible">
    <xsl:element name="fo:block" use-attribute-sets="sty.titre-article">
      <xsl:apply-templates select="self::node()"/>
    </xsl:element>
  </xsl:template>

  <xsl:template match="*" mode="id">
    <xsl:choose>
      <xsl:when test="@xml:id">
        <xsl:value-of select="@xml:id"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="generate-id()"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <xsl:template match="db:section | db:simplesect">
    <xsl:variable name="id">
      <xsl:apply-templates select="." mode="id"/>
    </xsl:variable>
    <fo:block xsl:use-attribute-sets="sty.section">
      <xsl:attribute name="id">
        <xsl:value-of select="$id"/>
      </xsl:attribute>
      <xsl:apply-templates/>
    </fo:block>
  </xsl:template>

  <xsl:template match="db:section/db:info/db:title | db:simplesect/db:info/db:title">
    <fo:block xsl:use-attribute-sets="sty.titre-section">
      <xsl:apply-templates/>
    </fo:block>
  </xsl:template>

  <xsl:template match="db:section/db:info/db:abstract | db:simplesect/db:info/db:abstract">
    <fo:block>
      <xsl:apply-templates/>
    </fo:block>
  </xsl:template>

  <xsl:template match="db:sidebar">
    <xsl:variable name="id">
      <xsl:apply-templates select="." mode="id"/>
    </xsl:variable>
    <fo:block-container xsl:use-attribute-sets="sty.encadré">
      <xsl:attribute name="id">
        <xsl:value-of select="$id"/>
      </xsl:attribute>
      <xsl:apply-templates/>
    </fo:block-container>
  </xsl:template>

  <xsl:template match="db:sidebar/db:info/db:title">
    <fo:block xsl:use-attribute-sets="sty.titre-encadré">
      <xsl:apply-templates/>
    </fo:block>
  </xsl:template>

  <xsl:template match="db:simpara | db:para">
    <xsl:choose>
      <xsl:when test="parent::*[self::db:section or self::db:simplesect]">
        <xsl:element name="fo:block" use-attribute-sets="sty.paragraphe">
          <xsl:apply-templates/>
        </xsl:element>
      </xsl:when>
      <xsl:otherwise>
        <xsl:element name="fo:block">
          <xsl:apply-templates/>
        </xsl:element>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <xsl:template match="db:literallayout | db:programlisting">
    <xsl:element name="fo:block" use-attribute-sets="sty.code.bloc">
      <xsl:apply-templates/>
    </xsl:element>
  </xsl:template>

  <xsl:template match="db:footnote/db:simpara | db:caption/db:simpara">
    <xsl:apply-templates/>
  </xsl:template>


  <!-- objets mmedia -->

  <xsl:template match="db:mediaobject" mode="info-visible">
    <xsl:apply-templates select="self::node()"/>
  </xsl:template>

  <xsl:template match="db:mediaobject">
    <xsl:param name="caption-align" select="false()"/>
    <!--FIXME: faire un test pour sélectionner l'alignement de la légende-->
    <xsl:param name="largeur">
      <xsl:call-template name="gen-largeur-illustration"/>
    </xsl:param>
    <xsl:choose>
      <xsl:when test="(ancestor::db:section) and db:caption and $caption-align">
        <xsl:call-template name="gen-alignement">
          <xsl:with-param name="alignement-vertical" select="'after'"/>
          <xsl:with-param name="indentation" select="$largeur"/>
          <xsl:with-param name="label">
            <xsl:apply-templates select="db:imageobject"/>
          </xsl:with-param>
          <xsl:with-param name="contenu">
            <xsl:apply-templates select="db:caption"/>
          </xsl:with-param>
        </xsl:call-template>
      </xsl:when>
      <xsl:otherwise>
        <xsl:element name="fo:block" use-attribute-sets="sty.illustration">
          <xsl:apply-templates select="db:imageobject"/>
          <xsl:apply-templates select="db:caption"/>
        </xsl:element>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <xsl:template match="db:imageobject">
    <xsl:element name="fo:block" use-attribute-sets="sty.image">
      <xsl:apply-templates/>
    </xsl:element>
  </xsl:template>

  <xsl:template match="db:caption">
    <xsl:element name="fo:block" use-attribute-sets="sty.description-image">
      <xsl:apply-templates/>
    </xsl:element>
  </xsl:template>

  <xsl:template match="db:imagedata">
    <xsl:param name="largeur">
      <xsl:call-template name="gen-largeur-illustration"/>
    </xsl:param>
    <xsl:element name="fo:external-graphic">
      <xsl:attribute name="src">
        <xsl:value-of select="concat('url(',attribute::fileref,')')"/>
      </xsl:attribute>
      <xsl:attribute name="width">
        <xsl:value-of select="$largeur"/>
      </xsl:attribute>
      <xsl:attribute name="content-width">
        <xsl:text>scale-to-fit</xsl:text>
      </xsl:attribute>
    </xsl:element>
  </xsl:template>

  <xsl:template match="db:itemizedlist">
    <xsl:element name="fo:list-block" use-attribute-sets="sty.liste">
      <xsl:apply-templates/>
    </xsl:element>
  </xsl:template>

  <xsl:template match="db:listitem">
    <xsl:element name="fo:list-item" use-attribute-sets="sty.liste.item">
      <xsl:element name="fo:list-item-label" use-attribute-sets="sty.liste.item.label">
        <xsl:element name="fo:block">-</xsl:element>
      </xsl:element>
      <xsl:element name="fo:list-item-body" use-attribute-sets="sty.liste.item.corps">
        <xsl:for-each select="db:para | db:simpara">
          <xsl:element name="fo:block">
            <xsl:apply-templates/>
          </xsl:element>
        </xsl:for-each>
      </xsl:element>
    </xsl:element>
  </xsl:template>

  <!-- == générateurs == -->

  <xsl:template name="gen-tête">
    <xsl:for-each select="db:info">
      <xsl:element name="fo:block-container" use-attribute-sets="sty.tête-article">
        <xsl:attribute name="id">
          <xsl:apply-templates select="//db:article" mode="id"/>
        </xsl:attribute>
        <xsl:attribute name="span">all</xsl:attribute>

        <xsl:apply-templates mode="info-visible" select="db:title"/>

        <xsl:choose>

          <xsl:when test="db:mediaobject">
            <xsl:call-template name="gen-alignement">
              <xsl:with-param name="indentation">
                <xsl:call-template name="gen-largeur-illustration"/>
              </xsl:with-param>
              <xsl:with-param name="label">
                <xsl:apply-templates mode="info-visible" select="db:mediaobject[1]"/>
              </xsl:with-param>
              <xsl:with-param name="contenu">
                <xsl:call-template name="gen-bloc-résumé"/>
                <xsl:call-template name="gen-bloc-méta"/>
              </xsl:with-param>
            </xsl:call-template>
          </xsl:when>

          <xsl:otherwise>
            <xsl:call-template name="gen-bloc-résumé"/>
            <xsl:call-template name="gen-bloc-méta"/>
          </xsl:otherwise>

        </xsl:choose>
      </xsl:element>
    </xsl:for-each>
  </xsl:template>

  <xsl:template name="gen-bloc-méta">
    <!-- Ce modèle détermine la disposition des méta-informations visibles.-->
    <!-- fixme: faire des modes bloc|ligne|meta -->
    <xsl:element name="fo:block" use-attribute-sets="sty.métabloc">
      <fo:block>
        <xsl:apply-templates mode="info-visible" select="db:authorgroup"/>
        <xsl:if test="db:editor">
          <xsl:text>, </xsl:text>
          <xsl:apply-templates mode="info-visible" select="db:editor"/>
        </xsl:if>
        <xsl:if test="db:publishername">
          <xsl:text> | </xsl:text>
          <xsl:apply-templates mode="info-visible" select="db:publishername"/>
        </xsl:if>
      </fo:block>
      <fo:block>
        <xsl:apply-templates mode="info-visible" select="db:pubdate"/>
      </fo:block>
      <fo:block>
        <xsl:apply-templates mode="info-visible" select="db:subjectset"/>
      </fo:block>
      <fo:block>
        <xsl:apply-templates mode="info-visible" select="db:keywordset"/>
      </fo:block>
    </xsl:element>
  </xsl:template>

  <xsl:template name="gen-bloc-résumé">
    <xsl:element name="fo:block" use-attribute-sets="sty.résumé-article">
      <xsl:choose>
        <xsl:when test="not(db:abstract)">
          <xsl:apply-templates select="//db:simpara[1]"/>
        </xsl:when>
        <xsl:otherwise>
          <xsl:apply-templates mode="info-visible" select="db:abstract"/>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:element>
  </xsl:template>

  <xsl:template name="gen-corps">
    <xsl:element name="fo:block-container" use-attribute-sets="sty.corps-article">
      <xsl:apply-templates select="node()[not(self::db:info)]"/>
    </xsl:element>
  </xsl:template>

  <xsl:template name="gen-static-content">
    <fo:static-content flow-name="xsl-region-before">
      <fo:block xsl:use-attribute-sets="sty.tête-page">
        <xsl:value-of select="$doc.title"/>
        <xsl:text> &#8212; </xsl:text>
        <xsl:call-template name="gen-locale">
          <xsl:with-param name="nom">page</xsl:with-param>
        </xsl:call-template>
        <fo:page-number/>
        <xsl:text> / </xsl:text>
        <fo:page-number-citation ref-id="dernière-page"/>
      </fo:block>
    </fo:static-content>

    <fo:static-content flow-name="xsl-region-after">
      <fo:block xsl:use-attribute-sets="sty.pied-page">
        <xsl:if test="$doc.copyright != ''">
          <xsl:value-of select="$doc.copyright"/>
          <xsl:text> &#8212; </xsl:text>
        </xsl:if>
        <xsl:call-template name="gen-xslt-liseret"/>
      </fo:block>
    </fo:static-content>

  </xsl:template>

  <xsl:template name="gen-signature">
    <xsl:if test="$prm.signature">
      <xsl:element name="fo:block" use-attribute-sets="sty.signature">
        <xsl:value-of select="$doc.author"/>
        <xsl:if test="$prm.signature.svg">
          <xsl:text> </xsl:text>
          <xsl:call-template name="gen-svg-rect">
            <xsl:with-param name="largeur">6pt</xsl:with-param>
            <xsl:with-param name="hauteur">6pt</xsl:with-param>
          </xsl:call-template>
        </xsl:if>
      </xsl:element>
    </xsl:if>
  </xsl:template>

  <xsl:template name="gen-bloc-notes">
    <!-- FIXME Ajouter les définitions d'abréviations sur le même modèle-->
    <!-- FIXME Ajouter les liens sur le même modèle-->
    <xsl:param name="notes">
      <xsl:for-each select="//db:footnote | //db:termdef">
        <xsl:element name="fo:list-item" use-attribute-sets="sty.note">
          <xsl:element name="fo:list-item-label">
            <xsl:attribute name="end-indent">label-end()</xsl:attribute>
            <xsl:element name="fo:block" use-attribute-sets="sty.note.label">
              <xsl:number count="//db:termdef | //db:footnote" level="any"/>
            </xsl:element>
          </xsl:element>
          <xsl:element name="fo:list-item-body">
            <xsl:attribute name="start-indent">body-start()</xsl:attribute>
            <xsl:element name="fo:block" use-attribute-sets="sty.note.corps">
              <xsl:apply-templates/>
            </xsl:element>
          </xsl:element>
        </xsl:element>
      </xsl:for-each>
    </xsl:param>
    <xsl:if test="normalize-space($notes) != ''">
      <xsl:element name="fo:list-block" use-attribute-sets="sty.bloc-notes">
        <xsl:copy-of select="$notes"/>
      </xsl:element>
    </xsl:if>
  </xsl:template>

  <xsl:template mode="gen-signet" match="*">
    <xsl:param name="titre">
      <xsl:apply-templates select="db:info/db:title"/>
    </xsl:param>
    <xsl:param name="id">
      <xsl:apply-templates select="." mode="id"/>
    </xsl:param>

    <fo:bookmark>
      <xsl:attribute name="internal-destination">
        <xsl:value-of select="$id"/>
      </xsl:attribute>

      <fo:bookmark-title>
        <xsl:value-of select="normalize-space($titre)"/>
      </fo:bookmark-title>

      <xsl:apply-templates mode="gen-signet" select="db:section|db:simplesect"/>

    </fo:bookmark>
  </xsl:template>

  <xsl:template name="gen-alignement">
    <xsl:param name="indentation"/>
    <xsl:param name="label"/>
    <xsl:param name="contenu"/>
    <xsl:param name="alignement-vertical">before</xsl:param>
    <xsl:param name="mode">table</xsl:param>

    <xsl:choose>
      <xsl:when test="$mode='liste'">
        <xsl:call-template name="gen-alignement-liste">
          <xsl:with-param name="indentation" select="$indentation"/>
          <xsl:with-param name="label">
            <xsl:copy-of select="$label"/>
          </xsl:with-param>
          <xsl:with-param name="contenu">
            <xsl:copy-of select="$contenu"/>
          </xsl:with-param>
        </xsl:call-template>
      </xsl:when>
      <xsl:when test="$mode='table'">
        <xsl:call-template name="gen-alignement-table">
          <xsl:with-param name="indentation" select="$indentation"/>
          <xsl:with-param name="alignement-vertical" select="$alignement-vertical"/>
          <xsl:with-param name="label">
            <xsl:copy-of select="$label"/>
          </xsl:with-param>
          <xsl:with-param name="contenu">
            <xsl:copy-of select="$contenu"/>
          </xsl:with-param>
        </xsl:call-template>
      </xsl:when>
      <xsl:otherwise>
        <xsl:message>ERR: erreur dans l'alignement des blocs.</xsl:message>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <xsl:template name="gen-alignement-liste">
    <xsl:param name="indentation"/>
    <xsl:param name="label"/>
    <xsl:param name="contenu"/>

    <xsl:element name="fo:list-block">
      <xsl:attribute name="provisional-distance-between-starts">
        <xsl:value-of select="$indentation"/>
      </xsl:attribute>

      <xsl:element name="fo:list-item">

        <xsl:element name="fo:list-item-label">
          <xsl:attribute name="end-indent">label-end()</xsl:attribute>
          <xsl:copy-of select="$label"/>
        </xsl:element>

        <xsl:element name="fo:list-item-body">
          <xsl:attribute name="start-indent">body-start() + 1em</xsl:attribute>
          <xsl:copy-of select="$contenu"/>
        </xsl:element>

      </xsl:element>
    </xsl:element>
  </xsl:template>

  <xsl:template name="gen-alignement-table">
    <xsl:param name="indentation"/>
    <xsl:param name="label"/>
    <xsl:param name="contenu"/>
    <xsl:param name="alignement-vertical"/>
    <fo:table table-layout="fixed" width="100%">
      <fo:table-column>
        <xsl:attribute name="column-width">
          <xsl:value-of select="concat($indentation,' + 1em')"/>
        </xsl:attribute>
      </fo:table-column>
      <fo:table-column>
        <xsl:attribute name="column-width">
          <xsl:value-of select="concat('100% - (', $indentation,' + 1em)')"/>
        </xsl:attribute>
      </fo:table-column>
      <fo:table-body>
        <fo:table-row>
          <fo:table-cell>
            <xsl:copy-of select="$label"/>
          </fo:table-cell>
          <fo:table-cell display-align="{$alignement-vertical}">
            <xsl:copy-of select="$contenu"/>
          </fo:table-cell>
        </fo:table-row>
      </fo:table-body>
    </fo:table>
  </xsl:template>

  <xsl:template name="gen-locale">
    <!--FIXME: à isoler et à compléter dans une table -->
    <xsl:param name="nom" select="local-name(.)"/>
    <xsl:param name="locale">
      <xsl:choose>
        <xsl:when test="$nom = 'keywordset'">
          <xsl:choose>
            <xsl:when test="lang('fr')">mots-clés : </xsl:when>
            <xsl:when test="lang('it')">chiavi: </xsl:when>
            <xsl:when test="lang('es')">claves: </xsl:when>
            <xsl:otherwise>keywords: </xsl:otherwise>
          </xsl:choose>
        </xsl:when>
        <xsl:when test="$nom = 'subjectset'">
          <xsl:choose>
            <xsl:when test="lang('fr')">classé dans </xsl:when>
            <xsl:otherwise>subject: </xsl:otherwise>
          </xsl:choose>
        </xsl:when>
        <xsl:when test="$nom = 'editor'">
          <xsl:choose>
            <xsl:when test="lang('fr')">éditions </xsl:when>
            <xsl:when test="lang('it')">edizioni </xsl:when>
            <xsl:when test="lang('es')">ediciones </xsl:when>
            <xsl:otherwise>Editor: </xsl:otherwise>
          </xsl:choose>
        </xsl:when>
        <xsl:when test="$nom = 'authorgroup'">
          <xsl:choose>
            <xsl:when test="lang('fr')">par </xsl:when>
            <xsl:when test="lang('it')">da </xsl:when>
            <xsl:when test="lang('es')">por </xsl:when>
            <xsl:otherwise>by </xsl:otherwise>
          </xsl:choose>
        </xsl:when>
        <xsl:when test="$nom = 'abstract'">
          <xsl:choose>
            <xsl:when test="lang('fr')">résumé : </xsl:when>
            <xsl:when test="lang('it')">riassunto: </xsl:when>
            <xsl:otherwise>summary: </xsl:otherwise>
          </xsl:choose>
        </xsl:when>
        <xsl:when test="$nom = 'bibliosource'">
          <xsl:choose>
            <xsl:when test="lang('fr')">source : </xsl:when>
            <xsl:when test="lang('it')">sorgente: </xsl:when>
            <xsl:when test="lang('es')">fuente: </xsl:when>
            <xsl:otherwise>source: </xsl:otherwise>
          </xsl:choose>
        </xsl:when>
        <xsl:when test="$nom = 'pubdate'">
          <xsl:choose>
            <xsl:when test="lang('fr')">publié le </xsl:when>
            <xsl:when test="lang('it')">publicato il </xsl:when>
            <xsl:otherwise>published </xsl:otherwise>
          </xsl:choose>
        </xsl:when>
        <xsl:when test="$nom = 'copyright'">©</xsl:when>
        <xsl:when test="$nom = 'publishername'">
          <xsl:choose>
            <xsl:when test="lang('fr')">publié par </xsl:when>
            <xsl:when test="lang('it')">publicato da </xsl:when>
            <xsl:when test="lang('es')">publicado por </xsl:when>
            <xsl:otherwise>published by </xsl:otherwise>
          </xsl:choose>
        </xsl:when>
        <xsl:when test="$nom = 'issn'">ISSN </xsl:when>
        <xsl:when test="$nom = 'irn'">IRN </xsl:when>
        <xsl:when test="$nom = 'page'">
          <xsl:choose>
            <xsl:when test="lang('es')">pagina </xsl:when>
            <xsl:when test="lang('it')">pagina </xsl:when>
            <xsl:otherwise>page </xsl:otherwise>
          </xsl:choose>
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="$nom"/>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:param>
    <xsl:value-of select="$locale"/>
  </xsl:template>

  <xsl:template name="gen-xslt-liseret">
    <xsl:text>généré par </xsl:text>
    <xsl:call-template name="gen-lien">
      <xsl:with-param name="href" select="$xslt.url"/>
      <xsl:with-param name="contenu" select="$xslt.nom"/>
    </xsl:call-template>
  </xsl:template>

  <xsl:template name="gen-svg-rect">
    <xsl:param name="hauteur">8pt</xsl:param>
    <xsl:param name="largeur">8pt</xsl:param>
    <fo:instream-foreign-object>
      <svg:svg xmlns="http://www.w3.org/2000/svg" version="1.0" width="{$largeur}"
        height="{$hauteur}">
        <svg:rect width="{$largeur}" height="{$hauteur}" x="0" y="0" fill="black" stroke="none"/>
      </svg:svg>
    </fo:instream-foreign-object>
  </xsl:template>

  <xsl:template name="gen-méta">
    <xsl:if test="$prm.méta.XEP">
      <rx:meta-info>
        <xsl:element name="rx:meta-field">
          <xsl:attribute name="name">author</xsl:attribute>
          <xsl:attribute name="value">
            <xsl:value-of select="$doc.author"/>
          </xsl:attribute>
        </xsl:element>
        <xsl:element name="rx:meta-field">
          <xsl:attribute name="name">creator</xsl:attribute>
          <xsl:attribute name="value">
            <xsl:value-of select="$doc.processor"/>
          </xsl:attribute>
        </xsl:element>
        <xsl:element name="rx:meta-field">
          <xsl:attribute name="name">title</xsl:attribute>
          <xsl:attribute name="value">
            <xsl:value-of select="normalize-space($doc.title)"/>
          </xsl:attribute>
        </xsl:element>
        <xsl:element name="rx:meta-field">
          <xsl:attribute name="name">subject</xsl:attribute>
          <xsl:attribute name="value">
            <xsl:value-of select="normalize-space($doc.subject)"/>
          </xsl:attribute>
        </xsl:element>
        <xsl:element name="rx:meta-field">
          <xsl:attribute name="name">keywords</xsl:attribute>
          <xsl:attribute name="value">
            <xsl:value-of select="normalize-space($doc.keywords)"/>
          </xsl:attribute>
        </xsl:element>
      </rx:meta-info>
    </xsl:if>

    <xsl:if test="$prm.méta.DC">
      <xsl:element xmlns:adobe="adobe:ns:meta/" name="adobe:xmpmeta">
        <xsl:element xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#" name="rdf:RDF">
          <xsl:element xmlns:dc="http://purl.org/dc/elements/1.1/"
            xmlns:xmp="http://ns.adobe.com/xap/1.0/" xmlns:pdf="http://ns.adobe.com/pdf/1.3/"
            name="rdf:Description" rdf:about="This document">
            <xsl:element name="dc:title">
              <xsl:value-of select="normalize-space($doc.title)"/>
            </xsl:element>
            <xsl:element name="dc:creator">
              <xsl:value-of select="normalize-space($doc.author)"/>
            </xsl:element>
            <xsl:element name="dc:description">
              <xsl:value-of select="normalize-space($doc.subject)"/>
            </xsl:element>
            <xsl:element name="pdf:Keywords">
              <xsl:value-of select="normalize-space($doc.keywords)"/>
            </xsl:element>
            <xsl:element name="xmp:CreatorTool">
              <xsl:value-of select="$doc.processor"/>
            </xsl:element>
          </xsl:element>
        </xsl:element>
      </xsl:element>
    </xsl:if>
  </xsl:template>

  <xsl:template name="gen-largeur-illustration">
    <xsl:param name="attr-largeur" select="descendant-or-self::db:imagedata/attribute::width"/>
    <xsl:choose>
      <xsl:when test="$attr-largeur != ''">
        <xsl:value-of select="$attr-largeur"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:choose>
          <xsl:when test="ancestor-or-self::db:info/parent::db:article">
            <xsl:text>2in</xsl:text>
          </xsl:when>
          <xsl:otherwise>
            <xsl:text>100%</xsl:text>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

</xsl:transform>
