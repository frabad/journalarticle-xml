#!/usr/bin/env python3

"""convertisseur d'article XML et démonstration de pipeline XML

Utilisation :
  {} <document> <format>

Description :
  Crée un document au format <format> à partir de <document>.
  <document> doit être un document XML valide.
"""

import pathlib
import sys
try:
    import garden.sys, garden.tree, garden.lxml, garden.subprocess
except ImportError as e:
    print(e,"\nInstallez le module 'garden'."
          "\n<https://gitlab.com/frabad/garden/>")
    sys.exit(1)
"""
définition des actions possibles
"""
pipeline = garden.subprocess.Pipeline((
    garden.subprocess.Pipeline.Step("docbook", None, "xsltproc", "OASIS DocBook XML"),
    garden.subprocess.Pipeline.Step("html", "docbook", "xsltproc", "W3C eXtensible HTML"),
    garden.subprocess.Pipeline.Step("fo", "docbook", "xsltproc", "W3C XSL Formatting Objects"),
    garden.subprocess.Pipeline.Step("rtf", "fo", "fo2rtf", "Microsoft Word Rich Text Format"),
    garden.subprocess.Pipeline.Step("pdf", "fo", "fop", "Adobe Portable Document Format")
))

def check(source, outform=None):
    """vérification de la commande et des ressources invoquées"""
    source = pathlib.Path(source) if source else None
    outforms = ("Formats disponibles :\n"+pipeline.format())
    if not source:
        print(__doc__, "\nFormats disponibles :\n  ",
            ", ".join(pipeline.abilities))
    elif not source.exists():
        print(f"'{source}' est introuvable.")
    elif not outform: # uniquement pour vérification syntaxique et validation
        xml, error = garden.lxml.parse(source)
        print(f"'{source}' %s" %("est syntaxiquement correct." if xml
            else "n'est pas un document XML correctement formé :"))
        if error: print("\t",error)
        else:
            valid, error = garden.lxml.validate(xml,
                pathlib.Path("model/article.xsd").absolute())
            print(f"'{source}' est %s" %("valide." if valid else "invalide :"))
            if error: print("\t",error)
    elif outform not in pipeline.abilities:
        print("Le format de sortie '%s' est inconnu." %(outform or "aucun"))
        if len(pipeline.abilities) == 0: print("Installez xsltproc.")
        else: print(outforms)
    else:
        return True

def transform(source,outform):
    """gère une commande de transformation"""
    source = pathlib.Path(source)
    outname = source.with_suffix("." + outform)
    result = pathlib.Path(pipeline.run(source,outform))
    if result.exists() and result.stat().st_size > 0:
        result.rename(outname)
        print("'%s' a été généré correctement." % outname)
    else:
        print("[ERREUR] impossible de générer le fichier '%s'." % outname)

if __name__ == "__main__":
    usr = garden.sys.args_struct("_exe source outform")
    __doc__=__doc__.format(usr._exe)
    if check(usr.source, usr.outform):
        transform(usr.source, usr.outform)

