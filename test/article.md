---
url:       https://gitlab.com/frabad/journalarticle
lang:      fr
tags:      politique, comédie, sciences
authors:   Pierre Dalun
editor:    Pierre Assel
publisher: Pierre Poli
revisor:   Pierre Ponse
copyright: Pierre Affeu 2009
pubdate:   2009-09-09
license:   public domain
image:     https://xmlgraphics.apache.org/fop/images/layout.jpg
---

# Un article

  Un article est un document normalisé qui présente une information
  identifiée par des métadonnées. Cette information s'adresse à un
  public ciblé. Elle est résumée dans le **chapeau** de l'article
  et développée dans une ou plusieurs **sections**.


## Une section

  Une section rassemble des blocs de texte appelés "paragraphes".
  Un paragraphe développe une idée rédigée en une ou plusieurs
  phrases.
  
  Les idées développées dans les paragraphes sont identifiées par
  quelques mots-clés et rassemblés dans le *titre* de la section.
  
